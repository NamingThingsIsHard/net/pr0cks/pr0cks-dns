"""
pr0cks
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import logging
import socket
import struct
import time
from collections import OrderedDict

from dnslib import DNSRecord, QTYPE
from dnslib.server import DNSHandler, BaseResolver, DNSServer


class Pr0cksDnsServer(DNSServer):

    def __init__(self, resolver, cli_args, **kwargs):
        super().__init__(resolver, **kwargs)
        self.cli_args = cli_args


class ProxyResolver(BaseResolver):
    def __init__(self, address, port):
        self.address = address
        self.port = port

    def resolve(self, request, handler):
        if handler.protocol == 'udp':
            proxy_r = request.send(self.address, self.port)
        else:
            proxy_r = request.send(self.address, self.port, tcp=True)
        reply = DNSRecord.parse(proxy_r)
        return reply


class PassthroughDNSHandler(DNSHandler):
    DNS_CACHE = OrderedDict()
    DNS_CACHE_SIZE = 1000

    def get_reply(self, data):
        host, port = self.server.resolver.address, self.server.resolver.port
        args = self.server.cli_args
        request = DNSRecord.parse(data)

        domain = str(request.q.qname)
        qtype = str(QTYPE.get(request.q.qtype))
        index = domain + "/" + qtype
        if not args.no_cache and index in self.DNS_CACHE:
            if time.time() < self.DNS_CACHE[index][0]:
                logging.debug(
                    "[i] %s served value from cache: %s",
                    index, ', '.join([x.rdata for x in self.DNS_CACHE[index][1]])
                )
                rep = request.reply()
                rep.add_answer(*self.DNS_CACHE[index][1])
                return rep.pack()
        logging.debug(
            "[i] domain %s requested using TCP server %s",
            domain, args.dns_server
        )
        data = struct.pack("!H", len(data)) + data
        response = send_tcp(data, host, port)
        response = response[2:]
        reply = DNSRecord.parse(response)
        logging.debug(
            "[i] %s %s resolve to %s",
            domain,
            qtype, ', '.join(
                [x.rdata for x in reply.rr]
            )
        )
        ttl = 3600
        resource_record = reply.rr
        if resource_record:
            if isinstance(resource_record, list):
                resource_record = reply.rr[0]
            ttl = resource_record.ttl

        self.DNS_CACHE[index] = (int(time.time()) + ttl, reply.rr)
        if len(self.DNS_CACHE) > self.DNS_CACHE_SIZE:
            self.DNS_CACHE.popitem(last=False)
        return response


def send_tcp(data, host, port):
    """
        Helper function to send/receive DNS TCP request
        (in/out packets will have prepended TCP length header)
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(5)
    sock.connect((host, port))
    sock.sendall(data)
    response = sock.recv(8192)
    length = struct.unpack("!H", bytes(response[:2]))[0]
    while len(response) - 2 < length:
        response += sock.recv(8192)
    sock.close()
    return response
