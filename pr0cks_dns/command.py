"""
pr0cks
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import argparse
import sys

from dnslib.server import DNSLogger
from pr0cks_extension.cli import Pr0cksCommand

from .dns import ProxyResolver, PassthroughDNSHandler, Pr0cksDnsServer


class Pr0cksDnsCommand(Pr0cksCommand):
    """
    Adds a DNS server to pr0cks that send the DNS requests through the proxy
    TODO actually send data through the proxy and not just straight to the DNS Server
    """

    NAME = "dns"

    def _add_args(self):
        self.arg_group.add_argument('--dns-port', default=1053, type=int,
                                    help="dns port to listen on (default 1053)")
        self.arg_group.add_argument('--dns-server', default="8.8.8.8:53",
                                    help="ip:port of the DNS server to forward all DNS requests to "
                                         "using TCP through the proxy (default 8.8.8.8:53)")

    def execute(self, args: argparse.Namespace, bind_address: str):
        try:
            dns_srv, dns_port = args.dns_server.split(':', 1)
            dns_port = int(dns_port)
        except ValueError as value_error:
            self.log.error("[-] %s", value_error)
            self.log.error("[-] Invalid dns server : %s", args.dns_server)
            sys.exit(1)
        resolver = ProxyResolver(dns_srv, dns_port)
        handler = PassthroughDNSHandler  # if args.passthrough else DNSHandler
        logger = DNSLogger("request,reply,truncated,error", False)
        udp_server = Pr0cksDnsServer(
            resolver,
            args,
            port=args.dns_port,
            address=bind_address,
            logger=logger,
            handler=handler
        )
        udp_server.start_thread()
        self.log.info(
            "DNS server started on %s:%s forwarding all DNS trafic to %s:%s using TCP",
            bind_address, args.dns_port, dns_srv, dns_port
        )
